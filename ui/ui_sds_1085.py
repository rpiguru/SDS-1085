# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'sds_1085.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.sds_rc

class Ui_SDS1085(object):
    def setupUi(self, SDS1085):
        if not SDS1085.objectName():
            SDS1085.setObjectName(u"SDS1085")
        SDS1085.resize(1354, 886)
        SDS1085.setStyleSheet(u"#SDS1085 {\n"
"background-color: rgb(52, 117, 205);\n"
"}")
        self.actionSe_ttings = QAction(SDS1085)
        self.actionSe_ttings.setObjectName(u"actionSe_ttings")
        self.actionE_xit = QAction(SDS1085)
        self.actionE_xit.setObjectName(u"actionE_xit")
        self.centralwidget = QWidget(SDS1085)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setLayoutDirection(Qt.LeftToRight)
        self.centralwidget.setStyleSheet(u"#centralwidget {\n"
"background-color: rgb(52, 117, 205);\n"
"}")
        self.verticalLayout_4 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setPixmap(QPixmap(u":/img/img/Smart-e-white-logo-png.png"))

        self.verticalLayout_4.addWidget(self.label)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalSpacer = QSpacerItem(20, 60, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.layoutLeft = QVBoxLayout()
        self.layoutLeft.setSpacing(10)
        self.layoutLeft.setObjectName(u"layoutLeft")
        self.vertical_line_1 = QFrame(self.centralwidget)
        self.vertical_line_1.setObjectName(u"vertical_line_1")
        self.vertical_line_1.setFrameShape(QFrame.VLine)
        self.vertical_line_1.setFrameShadow(QFrame.Sunken)

        self.layoutLeft.addWidget(self.vertical_line_1)

        self.label_input = QLabel(self.centralwidget)
        self.label_input.setObjectName(u"label_input")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_input.sizePolicy().hasHeightForWidth())
        self.label_input.setSizePolicy(sizePolicy1)
        self.label_input.setMinimumSize(QSize(80, 0))
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_input.setFont(font)
        self.label_input.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.layoutLeft.addWidget(self.label_input)

        self.vertical_line_2 = QFrame(self.centralwidget)
        self.vertical_line_2.setObjectName(u"vertical_line_2")
        self.vertical_line_2.setFrameShape(QFrame.VLine)
        self.vertical_line_2.setFrameShadow(QFrame.Sunken)

        self.layoutLeft.addWidget(self.vertical_line_2)


        self.verticalLayout_2.addLayout(self.layoutLeft)


        self.horizontalLayout_2.addLayout(self.verticalLayout_2)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(20)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(70, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.line_3 = QFrame(self.centralwidget)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setMinimumSize(QSize(0, 30))
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_3)

        self.label_output = QLabel(self.centralwidget)
        self.label_output.setObjectName(u"label_output")
        sizePolicy1.setHeightForWidth(self.label_output.sizePolicy().hasHeightForWidth())
        self.label_output.setSizePolicy(sizePolicy1)
        self.label_output.setMinimumSize(QSize(0, 30))
        self.label_output.setFont(font)
        self.label_output.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout.addWidget(self.label_output)

        self.line_4 = QFrame(self.centralwidget)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_4)


        self.verticalLayout_3.addLayout(self.horizontalLayout)

        self.widget_2 = QWidget(self.centralwidget)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy2)
        self.verticalLayout = QVBoxLayout(self.widget_2)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.widget_5 = QWidget(self.widget_2)
        self.widget_5.setObjectName(u"widget_5")
        sizePolicy1.setHeightForWidth(self.widget_5.sizePolicy().hasHeightForWidth())
        self.widget_5.setSizePolicy(sizePolicy1)
        self.widget_5.setMinimumSize(QSize(70, 30))

        self.horizontalLayout_3.addWidget(self.widget_5)

        self.widget_3 = QWidget(self.widget_2)
        self.widget_3.setObjectName(u"widget_3")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.widget_3.sizePolicy().hasHeightForWidth())
        self.widget_3.setSizePolicy(sizePolicy3)
        self.widget_3.setMinimumSize(QSize(0, 30))
        self.layoutOutputPageButtons = QHBoxLayout(self.widget_3)
        self.layoutOutputPageButtons.setSpacing(0)
        self.layoutOutputPageButtons.setObjectName(u"layoutOutputPageButtons")
        self.layoutOutputPageButtons.setContentsMargins(0, 0, 0, 0)

        self.horizontalLayout_3.addWidget(self.widget_3)

        self.label_4 = QLabel(self.widget_2)
        self.label_4.setObjectName(u"label_4")
        sizePolicy1.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy1)
        self.label_4.setMinimumSize(QSize(70, 30))
        self.label_4.setMaximumSize(QSize(70, 30))
        font1 = QFont()
        font1.setPointSize(13)
        font1.setBold(False)
        font1.setWeight(50)
        self.label_4.setFont(font1)
        self.label_4.setStyleSheet(u"color: rgb(255, 255, 255);")
        self.label_4.setAlignment(Qt.AlignBottom|Qt.AlignHCenter)

        self.horizontalLayout_3.addWidget(self.label_4)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.widget_4 = QWidget(self.widget_2)
        self.widget_4.setObjectName(u"widget_4")
        sizePolicy4 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.widget_4.sizePolicy().hasHeightForWidth())
        self.widget_4.setSizePolicy(sizePolicy4)
        self.widget_4.setMinimumSize(QSize(70, 0))
        self.widget_4.setMaximumSize(QSize(70, 16777215))
        self.layoutInputPageButtons = QVBoxLayout(self.widget_4)
        self.layoutInputPageButtons.setSpacing(0)
        self.layoutInputPageButtons.setObjectName(u"layoutInputPageButtons")
        self.layoutInputPageButtons.setContentsMargins(0, 0, 0, 0)

        self.horizontalLayout_4.addWidget(self.widget_4)

        self.frame = QFrame(self.widget_2)
        self.frame.setObjectName(u"frame")
        sizePolicy2.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy2)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setLineWidth(1)
        self.verticalLayout_6 = QVBoxLayout(self.frame)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.widget_6 = QWidget(self.frame)
        self.widget_6.setObjectName(u"widget_6")
        sizePolicy2.setHeightForWidth(self.widget_6.sizePolicy().hasHeightForWidth())
        self.widget_6.setSizePolicy(sizePolicy2)
        self.horizontalLayout_5 = QHBoxLayout(self.widget_6)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5 = QVBoxLayout()
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.widget_7 = QWidget(self.widget_6)
        self.widget_7.setObjectName(u"widget_7")
        sizePolicy1.setHeightForWidth(self.widget_7.sizePolicy().hasHeightForWidth())
        self.widget_7.setSizePolicy(sizePolicy1)
        self.widget_7.setMinimumSize(QSize(45, 30))
        self.widget_7.setMaximumSize(QSize(45, 16777215))

        self.verticalLayout_5.addWidget(self.widget_7)

        self.widget_9 = QWidget(self.widget_6)
        self.widget_9.setObjectName(u"widget_9")
        sizePolicy4.setHeightForWidth(self.widget_9.sizePolicy().hasHeightForWidth())
        self.widget_9.setSizePolicy(sizePolicy4)
        self.widget_9.setMinimumSize(QSize(45, 0))
        self.widget_9.setMaximumSize(QSize(45, 16777215))
        self.layoutInputNumbers = QVBoxLayout(self.widget_9)
        self.layoutInputNumbers.setObjectName(u"layoutInputNumbers")
        self.layoutInputNumbers.setContentsMargins(0, 0, 5, 0)

        self.verticalLayout_5.addWidget(self.widget_9)


        self.horizontalLayout_5.addLayout(self.verticalLayout_5)

        self.verticalLayout_7 = QVBoxLayout()
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.widget_8 = QWidget(self.widget_6)
        self.widget_8.setObjectName(u"widget_8")
        sizePolicy3.setHeightForWidth(self.widget_8.sizePolicy().hasHeightForWidth())
        self.widget_8.setSizePolicy(sizePolicy3)
        self.widget_8.setMinimumSize(QSize(0, 30))
        self.layoutOutputNumbers = QHBoxLayout(self.widget_8)
        self.layoutOutputNumbers.setObjectName(u"layoutOutputNumbers")
        self.layoutOutputNumbers.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_7.addWidget(self.widget_8)

        self.frame_2 = QFrame(self.widget_6)
        self.frame_2.setObjectName(u"frame_2")
        sizePolicy2.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy2)
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.layoutSwitch = QHBoxLayout(self.frame_2)
        self.layoutSwitch.setSpacing(0)
        self.layoutSwitch.setObjectName(u"layoutSwitch")
        self.layoutSwitch.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_7.addWidget(self.frame_2)


        self.horizontalLayout_5.addLayout(self.verticalLayout_7)


        self.verticalLayout_6.addWidget(self.widget_6)


        self.horizontalLayout_4.addWidget(self.frame)

        self.widget_10 = QWidget(self.widget_2)
        self.widget_10.setObjectName(u"widget_10")
        sizePolicy5 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.widget_10.sizePolicy().hasHeightForWidth())
        self.widget_10.setSizePolicy(sizePolicy5)
        self.widget_10.setMinimumSize(QSize(70, 0))
        self.verticalLayout_8 = QVBoxLayout(self.widget_10)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 2, 0, 1)
        self.label_5 = QLabel(self.widget_10)
        self.label_5.setObjectName(u"label_5")
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMinimumSize(QSize(70, 30))
        self.label_5.setMaximumSize(QSize(70, 30))
        self.label_5.setFont(font1)
        self.label_5.setStyleSheet(u"color: rgb(255, 255, 255);")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.verticalLayout_8.addWidget(self.label_5)

        self.widget = QWidget(self.widget_10)
        self.widget.setObjectName(u"widget")
        self.layoutSetAll = QVBoxLayout(self.widget)
        self.layoutSetAll.setSpacing(0)
        self.layoutSetAll.setObjectName(u"layoutSetAll")
        self.layoutSetAll.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_8.addWidget(self.widget)


        self.horizontalLayout_4.addWidget(self.widget_10)


        self.verticalLayout.addLayout(self.horizontalLayout_4)


        self.verticalLayout_3.addWidget(self.widget_2)


        self.horizontalLayout_2.addLayout(self.verticalLayout_3)


        self.verticalLayout_4.addLayout(self.horizontalLayout_2)

        SDS1085.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(SDS1085)
        self.statusbar.setObjectName(u"statusbar")
        SDS1085.setStatusBar(self.statusbar)
        self.menuBar = QMenuBar(SDS1085)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 1354, 23))
        self.menu_File = QMenu(self.menuBar)
        self.menu_File.setObjectName(u"menu_File")
        SDS1085.setMenuBar(self.menuBar)

        self.menuBar.addAction(self.menu_File.menuAction())
        self.menu_File.addAction(self.actionSe_ttings)
        self.menu_File.addSeparator()
        self.menu_File.addAction(self.actionE_xit)

        self.retranslateUi(SDS1085)

        QMetaObject.connectSlotsByName(SDS1085)
    # setupUi

    def retranslateUi(self, SDS1085):
        SDS1085.setWindowTitle(QCoreApplication.translate("SDS1085", u"SDS-1085", None))
        self.actionSe_ttings.setText(QCoreApplication.translate("SDS1085", u"Se&ttings", None))
        self.actionE_xit.setText(QCoreApplication.translate("SDS1085", u"E&xit", None))
        self.label.setText("")
        self.label_input.setText(QCoreApplication.translate("SDS1085", u"INPUTS", None))
        self.label_output.setText(QCoreApplication.translate("SDS1085", u"OUTPUTS", None))
        self.label_4.setText(QCoreApplication.translate("SDS1085", u"SET", None))
        self.label_5.setText(QCoreApplication.translate("SDS1085", u"ALL", None))
        self.menu_File.setTitle(QCoreApplication.translate("SDS1085", u"&File", None))
    # retranslateUi

