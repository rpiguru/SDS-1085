# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settings_dlg.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_SettingsDialog(object):
    def setupUi(self, SettingsDialog):
        if not SettingsDialog.objectName():
            SettingsDialog.setObjectName(u"SettingsDialog")
        SettingsDialog.resize(469, 285)
        font = QFont()
        font.setPointSize(12)
        SettingsDialog.setFont(font)
        self.verticalLayout = QVBoxLayout(SettingsDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(SettingsDialog)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.spinInputCount = QSpinBox(SettingsDialog)
        self.spinInputCount.setObjectName(u"spinInputCount")
        self.spinInputCount.setMinimum(1)
        self.spinInputCount.setMaximum(128)

        self.horizontalLayout.addWidget(self.spinInputCount)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_2 = QLabel(SettingsDialog)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_2.addWidget(self.label_2)

        self.spinOutputCount = QSpinBox(SettingsDialog)
        self.spinOutputCount.setObjectName(u"spinOutputCount")
        self.spinOutputCount.setMinimum(1)
        self.spinOutputCount.setMaximum(128)

        self.horizontalLayout_2.addWidget(self.spinOutputCount)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(20)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer = QSpacerItem(40, 10, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.btnOk = QPushButton(SettingsDialog)
        self.btnOk.setObjectName(u"btnOk")

        self.horizontalLayout_3.addWidget(self.btnOk)

        self.btnCancel = QPushButton(SettingsDialog)
        self.btnCancel.setObjectName(u"btnCancel")

        self.horizontalLayout_3.addWidget(self.btnCancel)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addLayout(self.horizontalLayout_3)


        self.retranslateUi(SettingsDialog)

        QMetaObject.connectSlotsByName(SettingsDialog)
    # setupUi

    def retranslateUi(self, SettingsDialog):
        SettingsDialog.setWindowTitle(QCoreApplication.translate("SettingsDialog", u"Settings", None))
        self.label.setText(QCoreApplication.translate("SettingsDialog", u"Input Cards:", None))
        self.label_2.setText(QCoreApplication.translate("SettingsDialog", u"Onput Cards:", None))
        self.btnOk.setText(QCoreApplication.translate("SettingsDialog", u"Ok", None))
        self.btnCancel.setText(QCoreApplication.translate("SettingsDialog", u"Cancel", None))
    # retranslateUi

