import threading
import time

import serial.tools.list_ports
import serial

from settings import MDX_BAUDRATE, DEBUG
from utils.logger import logger


class MDX(object):

    def __init__(self):
        self._ser = None
        self._lock = threading.Lock()
        self.connect()

    def connect(self):
        for port in serial.tools.list_ports.comports():
            try:
                self._ser = serial.Serial(port=port.device, baudrate=MDX_BAUDRATE, timeout=1)
                self._ser.reset_input_buffer()
                self._ser.reset_output_buffer()
                self._ser.write('/^Version;'.encode())
                response = self._ser.readline().decode()
                if "Ver" in response:
                    logger.info(f"Found an MDX adapter - {port.device}, version: {response}")
                    return True
                else:
                    self._ser.close()
                    self._ser = None
            except Exception as e:
                logger.error(f"Failed to open the MDX serial port - {e}")
                self._ser = None

    def _execute_cmd(self, cmd: str, read_all=False, show_log=True):
        response = ''
        with self._lock:
            if self._ser is not None or self.connect():
                if show_log:
                    logger.debug(f"MDX: Executing command - {cmd}")
                try:
                    if read_all:
                        self._ser.write(cmd.encode())
                        time.sleep(.4)
                        response = self._ser.read_all().decode()
                    else:
                        self._ser.reset_output_buffer()
                        self._ser.reset_input_buffer()
                        self._ser.write(cmd.encode())
                        response = self._ser.readline().decode()
                except Exception as e:
                    logger.error(f"MDX: Failed to read - {e}")
                    self._ser = None
        return response

    def get_version(self):
        """
        <Ver1.5>\r\n
        :return:
        """
        return self._execute_cmd('/^Version;')

    def get_status(self, verbose=True) -> dict:
        """
        'V:4   ->   1 ;', 'V:4   ->   2 ;', 'V:4   ->   3 ;'
        :return: `Output: Input` pairs.
        """
        if not DEBUG:
            response = self._execute_cmd('Status.', read_all=True, show_log=False)
            table = {}
            for line in [line.strip()[:-1] for line in response.splitlines()]:
                vals = [v.strip() for v in line.split('->')]
                try:
                    table[int(vals[1])] = int(vals[0][2:])
                except (ValueError, IndexError):
                    pass
            if verbose:
                logger.debug(f"MDX Status: {', '.join([f'{k}: {table[k]}' for k in sorted(table.keys())])}")
            return table
        elif DEBUG:
            return {4: 2, 1: 3, 3: 3, 2: 4, 8: 4, 5: 5, 6: 6, 7: 7}

    def switch_channel(self, i_in, i_out):
        return self._execute_cmd(f"{i_in}v{i_out}.")

    def switch_all_channels(self, i_in):
        return self._execute_cmd(f"{i_in}All.")

    def set_return_response_path(self, val):
        return self._execute_cmd('/:MessageOn;' if val else '/:MessageOff;')

    def reset_control_card(self):
        return self._execute_cmd('$Default!')

    def reset_input(self, channel=0):
        return self._execute_cmd(f"${channel}DefaultIn!")

    def reset_output(self, channel=0):
        return self._execute_cmd(f"${channel}DefaultOut!")

    def reset_all_inputs(self):
        return self._execute_cmd("$AllDefaultIn!")

    def reset_all_outputs(self):
        return self._execute_cmd("$AllDefaultOut!")

    def save_to_memory(self, location):
        return self._execute_cmd(f"Save{location}.")

    def recall_from_memory(self, location):
        return self._execute_cmd(f"Recall{location}.")

    def clear_memory_state(self, location):
        return self._execute_cmd(f"Clear{location}.")

    def set_fan_temperature(self, temp):
        return self._execute_cmd(f"FanTemp{temp}.")

    def close(self):
        if self._ser is not None:
            self._ser.close()
            self._ser = None
