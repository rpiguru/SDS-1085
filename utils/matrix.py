import threading

import serial.tools.list_ports
import serial

from settings import MAT_BAUDRATE
from utils.logger import logger


HEADER1 = 0xBE
HEADER2 = 0xEF


class Matrix(object):

    def __init__(self):
        self._ser = None
        self._lock = threading.Lock()
        self.connect()

    def connect(self):
        for port in serial.tools.list_ports.comports():
            try:
                self._ser = serial.Serial(port=port.device, baudrate=MAT_BAUDRATE, timeout=1)
                self._ser.reset_input_buffer()
                self._ser.reset_output_buffer()
                # Get matrix status at address 0
                cmd = bytearray([HEADER1, HEADER2, 0xA0, 0x00, 0x08, 0xFF])
                cmd = self._compose_cmd(cmd)
                self._ser.write(bytes(cmd))
                response = self._ser.readline()
                if HEADER1 in response and HEADER2 in response:
                    logger.info(f"Found a matrix adapter - {port.device}")
                    return True
                else:
                    self._ser.close()
                    self._ser = None
            except Exception as e:
                logger.error(f"Failed to open the matrix serial port - {e}")
                self._ser = None

    def is_connected(self):
        return self._ser is not None

    @staticmethod
    def _compose_cmd(cmd):
        chksum = 0x00
        for v in cmd:
            chksum = chksum ^ v
        cmd.append(chksum)
        return cmd

    def _send_cmd(self, cmd: bytearray, read_response=False):
        cmd = self._compose_cmd(cmd)
        response = None
        with self._lock:
            if self._ser is not None or self.connect():
                logger.debug(f"Matrix: Writing a command - {' '.join(['%02X' % v for v in cmd])}")
                self._ser.reset_output_buffer()
                self._ser.reset_input_buffer()
                self._ser.write(bytes(cmd))
                if read_response:
                    response = self._ser.readline()
        return response

    def get_matrix_status(self, addr=0):
        cmd = bytearray([HEADER1, HEADER2, 0xA0 + addr, 0x00, 0x08, 0xFF])
        return self._send_cmd(cmd, read_response=True)

    def set_channel(self, ch_in, ch_out):
        cmd = bytearray([HEADER1, HEADER2, 0xA0 + ch_out // 8, 0x00, 0x00, ch_in, ch_out % 8])
        self._send_cmd(cmd)

    def set_8_channels(self, address: int, values: list):
        # Set 8 channels by Switching Salvo command.
        cmd = bytearray([HEADER1, HEADER2, 0xA0 + address, 0x00, 0x09] + values)
        self._send_cmd(cmd)

    def close(self):
        if self._ser is not None:
            self._ser.close()
            self._ser = None
