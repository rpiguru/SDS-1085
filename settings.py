import json
import os

_cur_dir = os.path.dirname(os.path.realpath(__file__))

ROOT_DIR = os.path.expanduser('~/.sds')
os.makedirs(ROOT_DIR, exist_ok=True)

CONFIG_FILE = os.path.join(ROOT_DIR, 'config.json')
if not os.path.exists(CONFIG_FILE):
    print("No config found! Creating the default one...")
    default_data = {"input_count": 8, "output_count": 8}
    with open(CONFIG_FILE, 'w') as jp:
        json.dump(default_data, jp, indent=2)


MDX_BAUDRATE = 9600

MAT_BAUDRATE = 9600

DEFAULT_CNT = 16
MDX_INTERVAL = 2

BLUE_COLOR = "rgb(52, 117, 205)"

DEBUG = False

try:
    from local_settings import *
except ImportError:
    pass
