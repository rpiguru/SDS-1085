# SDS-1085

SDS-1085 GUI App

## Installation on Windows

```shell script
    python -m pip install -U pip setuptools wheel
    python -m pip install -r requirements.txt
  
```

## Package the application

- Create an executable: 

    ```shell script
    cp utils\hook-fastprogress.py venv_win\Lib\site-packages\PyInstaller\hooks
    venv_win\Scripts\pyinstaller.exe main.spec
    cp -R venv_win\Lib\site-packages\PySide2\plugins\platforms dist\SDS-1085
    ```

- Open [SDS-1085.ifp](SDS-1085.ifp) by [InstallForge](https://installforge.net/) and build a setup file.
