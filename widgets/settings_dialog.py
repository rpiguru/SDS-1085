from PySide2.QtCore import Signal
from PySide2.QtWidgets import QDialog

from ui.ui_settings_dlg import Ui_SettingsDialog
from utils.common import get_config, update_config_file


class SettingsDialog(QDialog):

    closed = Signal()

    def __init__(self, parent):
        super().__init__(parent=parent)
        self.ui = Ui_SettingsDialog()
        self.ui.setupUi(self)
        conf = get_config()
        self.ui.spinInputCount.setValue(conf['input_count'])
        self.ui.spinOutputCount.setValue(conf['output_count'])
        self.ui.btnCancel.released.connect(self.close)
        self.ui.btnOk.released.connect(self._on_btn_ok)

    def _on_btn_ok(self):
        update_config_file({
            'input_count': self.ui.spinInputCount.value(),
            'output_count': self.ui.spinOutputCount.value()
        })
        self.close()

    def closeEvent(self, event):
        getattr(self.closed, "emit")()
