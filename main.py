import math
import sys
import os
import threading
import time
from functools import partial

from utils.common import get_config
from widgets.message import show_message
from widgets.settings_dialog import SettingsDialog

os.environ['QT_API'] = 'pyside2'

from settings import MDX_INTERVAL, DEFAULT_CNT, BLUE_COLOR
from utils.matrix import Matrix
from utils.mdx import MDX
from PySide2.QtCore import Signal, QSize
from PySide2.QtGui import Qt, QIcon
from PySide2.QtWidgets import QMainWindow, QApplication, QPushButton, QSizePolicy, QSpacerItem, QLabel, QVBoxLayout, \
    QLayout

from ui.ui_sds_1085 import Ui_SDS1085
from utils.logger import logger


class SDS1085App(QMainWindow):

    mdx_data = Signal(dict)

    def __init__(self):
        super().__init__()
        self.ui = Ui_SDS1085()
        self.ui.setupUi(self)
        self.conf = get_config()
        self.setFixedSize(self.size())
        self.ui.layoutLeft.setAlignment(self.ui.vertical_line_1, Qt.AlignHCenter)
        self.ui.layoutLeft.setAlignment(self.ui.vertical_line_2, Qt.AlignHCenter)
        self._i_input = -1
        self._i_output = -1
        getattr(self, "mdx_data").connect(self._on_mdx_data_received)
        self.status = {}
        self._stop = threading.Event()
        self._stop.clear()

        self.mdx = MDX()
        self.matrix = Matrix()

        self._add_page_buttons()

        self._tr_mdx = threading.Thread(target=self._get_mdx_status)
        self._tr_mdx.start()
        self.ui.actionE_xit.triggered.connect(self.close)
        self.ui.actionSe_ttings.triggered.connect(self._on_action_settings)
        self._last_switch_time = time.time()

    def _add_page_buttons(self):
        self.clear_layout(self.ui.layoutOutputPageButtons)
        self.clear_layout(self.ui.layoutInputPageButtons)
        self.status = self.mdx.get_status()
        for addr in range(self.conf['output_count'] // 8 + 1):
            self.matrix.set_8_channels(address=addr,
                                       values=[self.status.get(v, 0) for v in range(8 * addr, 8 * addr + 1)])
            time.sleep(.01)

        for p in range(math.ceil(self.conf['output_count'] / DEFAULT_CNT)):
            btn = QPushButton(parent=self, text=f"Page {p + 1}")
            size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
            btn.setSizePolicy(size_policy)
            btn.setMinimumSize(QSize(70, 30))
            btn.setMaximumSize(QSize(70, 30))
            btn.released.connect(partial(self._on_page_button, 'output', p))
            self.ui.layoutOutputPageButtons.addWidget(btn)
        self.ui.layoutOutputPageButtons.addItem(QSpacerItem(100, 30, QSizePolicy.Expanding, QSizePolicy.Minimum))
        for p in range(math.ceil(self.conf['input_count'] / DEFAULT_CNT)):
            btn = QPushButton(parent=self, text=f"Page {p + 1}")
            size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
            btn.setSizePolicy(size_policy)
            btn.setMinimumSize(QSize(70, 30))
            btn.released.connect(partial(self._on_page_button, 'input', p))
            self.ui.layoutInputPageButtons.addWidget(btn)
        self.ui.layoutInputPageButtons.addItem(QSpacerItem(70, 30, QSizePolicy.Fixed, QSizePolicy.Expanding))
        # Add channel labels
        self._on_page_button(b_type='output', index=0)
        self._on_page_button(b_type='input', index=0)

    def _get_mdx_status(self):
        while not self._stop.is_set():
            s_time = time.time()
            status = self.mdx.get_status(verbose=False)
            if not self.status or any([self.status.get(k, v) != v for k, v in status.items()]):
                logger.debug(f"Status changed - {status}")
                for k, v in status.items():
                    if v != self.status.get(k):
                        logger.info(f"Setting matrix: {v - 1} => {k - 1}")
                        self.matrix.set_channel(ch_in=v - 1, ch_out=k - 1)
                getattr(self, "mdx_data").emit(status)
            time.sleep(max(MDX_INTERVAL - time.time() + s_time, 0))
        self.mdx.close()

    def _update_page_labels(self, b_type='output'):
        layout = getattr(self.ui, f"layout{b_type.capitalize()}Numbers")
        self.clear_layout(layout)
        index = getattr(self, f"_i_{b_type}")
        max_cnt = self.conf[f"{b_type}_count"]
        for i in range(DEFAULT_CNT * index, min(DEFAULT_CNT * (index + 1), max_cnt)):
            label = QLabel(parent=self, text=str(i + 1))
            label.setStyleSheet("font: bold; color: white; font-size: 18px")
            layout.addWidget(label)
            layout.setAlignment(label, Qt.AlignRight if b_type == 'input' else Qt.AlignCenter)
        if b_type == 'input':
            self.clear_layout(self.ui.layoutSetAll)
            for i in range(DEFAULT_CNT * index, min(DEFAULT_CNT * (index + 1), max_cnt)):
                btn = QPushButton()
                btn.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred))
                btn.released.connect(partial(self._on_btn_set_all, btn, i + 1))
                self.ui.layoutSetAll.addWidget(btn)

    def _on_page_button(self, b_type='output', index=0):
        getattr(self.ui, f"label_{b_type}").setText(f"{b_type.capitalize()} ({index + 1})")
        if getattr(self, f"_i_{b_type}") != index:
            setattr(self, f"_i_{b_type}", index)
            self._update_page_labels(b_type=b_type)
            layout = getattr(self.ui, f"layout{b_type.capitalize()}PageButtons")
            for i in range(layout.count()):
                btn = layout.itemAt(i).widget()
                if btn is not None and isinstance(btn, QPushButton):
                    if int(btn.text().split(' ')[1]) == index + 1:
                        btn.setStyleSheet("font: bold; background-color: white; color: gray; font-size: 16px")
                    else:
                        btn.setStyleSheet(
                            f"font: normal; background-color: {BLUE_COLOR}; color: white; font-size: 14px")

            self._update_switch()

    def _on_mdx_data_received(self, status):
        for k, v in status.items():
            self.status[k] = v
        self._update_switch()

    def _update_switch(self):
        self.clear_layout(self.ui.layoutSwitch)
        for i_out in range(
                DEFAULT_CNT * self._i_output, min(DEFAULT_CNT * (self._i_output + 1), self.conf['output_count'])):
            layout = QVBoxLayout()
            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 0)
            for i_in in range(DEFAULT_CNT * self._i_input,
                              min(DEFAULT_CNT * (self._i_input + 1), self.conf['input_count'])):
                btn = QPushButton()
                btn.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred))
                btn.released.connect(partial(self._on_switch_btn_released, btn, i_out + 1, i_in + 1))
                if self.status.get(i_out + 1) == i_in + 1:
                    btn.setStyleSheet(f"background-color: {BLUE_COLOR};")
                layout.addWidget(btn)
            self.ui.layoutSwitch.addLayout(layout)

    def _on_switch_btn_released(self, btn, i_out, i_in):
        if self.status.get(i_out - 1) == i_in - 1:
            return
        elif time.time() - self._last_switch_time < .3:
            logger.warning("Pressed switch button to fast!")
            return
        self._last_switch_time = time.time()
        logger.info(f"Setting a switch {i_in} => {i_out}")
        btn.setStyleSheet(f"background-color: {BLUE_COLOR};")
        self.mdx.switch_channel(i_in=i_in, i_out=i_out)
        # Remove blue color from the old button
        self._remove_old_blue_color(i_out - 1)
        self.matrix.set_channel(ch_in=i_in - 1, ch_out=i_out - 1)
        for i in range(self.ui.layoutSetAll.count()):
            self.ui.layoutSetAll.itemAt(i).widget().setStyleSheet("")

    def _remove_old_blue_color(self, i_out):
        # Remove blue color from the old button
        old_input = self.status.get(i_out)
        if old_input is not None and DEFAULT_CNT * self._i_input <= old_input < DEFAULT_CNT * (self._i_input + 1):
            layout = self.ui.layoutSwitch.itemAt(i_out % DEFAULT_CNT)
            if layout is not None:
                item = layout.itemAt(old_input % DEFAULT_CNT)
                if item is not None and item.widget() is not None:
                    item.widget().setStyleSheet("")

    def _on_btn_set_all(self, btn, i_in):
        start = self._i_output * DEFAULT_CNT
        logger.info(f"Switching current outputs ({start} ~ {start + DEFAULT_CNT}) from {i_in}")

        for i_out in range(start, start + DEFAULT_CNT):
            self._remove_old_blue_color(i_out)
            layout = self.ui.layoutSwitch.itemAt(i_out % DEFAULT_CNT)
            if layout is not None:
                btn = layout.itemAt((i_in - 1) % DEFAULT_CNT).widget()
                if btn is not None:
                    btn.setStyleSheet(f"background-color: {BLUE_COLOR};")
        for i in range(self.ui.layoutSetAll.count()):
            if i != i_in - 1:
                self.ui.layoutSetAll.itemAt(i).widget().setStyleSheet("")
            else:
                btn.setStyleSheet(f"background-color: {BLUE_COLOR};")
        self.mdx.switch_channel(i_in=i_in,
                                i_out=','.join([str(v) for v in range(start + 1, start + DEFAULT_CNT + 1)]))
        self.matrix.set_8_channels(address=start * 2, values=[i_in - 1] * 8)
        self.matrix.set_8_channels(address=start * 2 + 1, values=[i_in - 1] * 8)

    def _on_action_settings(self):
        dlg = SettingsDialog(parent=self)
        dlg.closed.connect(self._on_settings_dlg_closed)
        dlg.open()

    def _on_settings_dlg_closed(self):
        if self.conf != get_config():
            show_message("Settings Changed, please launch this application again.")
            self.close()
        # self.conf = get_config()
        # self._add_page_buttons()
        # self._on_page_button(b_type='output', index=0)
        # self._on_page_button(b_type='input', index=0)

    def closeEvent(self, event):
        self._stop.set()
        self.mdx.close()
        self.matrix.close()
        self._tr_mdx.join(.1)
        super(SDS1085App, self).closeEvent(event)

    def on_crashed(self):
        pass

    def clear_layout(self, layout):
        for i in reversed(range(layout.count())):
            item = layout.itemAt(i)
            if item.widget() is not None:
                item.widget().deleteLater()
            elif isinstance(item, QLayout):
                self.clear_layout(item)
                layout.removeItem(item)


if __name__ == '__main__':

    logger.info('========== Starting SDS-1085 Application ==========')

    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('SDS-1085.ico'))
    # app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
    form = SDS1085App()

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, tb):
        logger.error("=========== Crashed!", exc_info=(exctype, value, tb))
        getattr(sys, "_excepthook")(exctype, value, tb)
        form.on_crashed()
        sys.exit(1)

    sys.excepthook = exception_hook

    form.show()
    sys.exit(app.exec_())
